package main

import (
	"mandapp/kit/command"
	"mandapp/kit/err"
	"mandapp/kit/vo"
	"net/http"

	"github.com/gin-gonic/gin"
)

func ReceiveOrder(c *gin.Context) {
	orderId := vo.UUID{}.Parse(c.Param("order_id"))
	customerId := vo.UUID{}.Parse(c.Param("customer_id"))
	var cmd command.ArriveOrder
	c.Bind(&cmd)
	cmd.OrderId = orderId
	cmd.CustomerId = customerId
	if !cmd.IsZero() {
		c.JSON(http.StatusBadRequest, err.InvalidJSONInput{}.Error())
		return
	}

	cmdBus.Dispatch(cmd)
	bodyRes := map[string]interface{}{"order_id": cmd.OrderId.String()}
	c.JSON(http.StatusOK, bodyRes)
}
