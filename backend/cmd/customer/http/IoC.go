package main

import "mandapp/kit/bus"

var (
	cmdBus bus.CommandBus
)

func SetUp() {
	cmdBus = bus.InMemoryCommandBus{}.Unique()
}
