package bus

import (
	"mandapp/kit/command"
)

type CommandBus interface {
	Dispatch(command.Command)
	Register(string, CommandHandler)
}
