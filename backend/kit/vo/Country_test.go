package vo_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"mandapp/kit/vo"
	"syreclabs.com/go/faker"
)

var _ = Describe("Country", func() {
	Context("empty context.", func() {
		It("should returns value passed as argument", func() {
			countryFake := faker.Address().Country()
			res := vo.Country{}.From(countryFake)

			Expect(countryFake).Should(Equal(res.String()))
		})
	})
})
