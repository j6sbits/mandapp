package vo_test

import (
	"encoding/json"
	"mandapp/kit/vo"

	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("UUID", func() {
	Context("empty context.", func() {
		It("should returns value passed as argument", func() {
			uuidFake, _ := uuid.NewRandom()
			res := vo.UUID{}.From(uuidFake)

			Expect(uuidFake.String()).Should(Equal(res.String()))
		})

		It("should parse json to uuid", func() {
			input := []byte(`{
				"customer_id": "87117205-e478-405b-8fab-fa9a506970e6"
			}`)
			data := struct {
				CustormerId vo.UUID `json:"customer_id"`
			}{}
			err := json.Unmarshal(input, &data)

			Expect(err).Should(BeNil())
			Expect(data.CustormerId.String()).Should(Equal("87117205-e478-405b-8fab-fa9a506970e6"))
		})
	})
})
