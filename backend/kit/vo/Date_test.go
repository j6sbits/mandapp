package vo_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"mandapp/kit/vo"
)

var _ = Describe("Date", func() {
	Context("empty context", func() {
		It("", func() {
			zeroDay := vo.Date{}.Default()

			Expect(zeroDay.IsZero()).To(BeTrue())
		})
	})
})
