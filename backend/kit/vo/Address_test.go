package vo_test

import (
	"fmt"
	"mandapp/kit/vo"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"syreclabs.com/go/faker"
)

var _ = Describe("Address", func() {
	Context("empty context.", func() {
		It("should returns the value passed as argument.", func() {
			addressFake := fmt.Sprintf(
				"%s %d # %d%s - %d",
				faker.RandomChoice([]string{"cra", "carrera", "cll", "calle"}),
				faker.RandomInt(1, 200),
				faker.RandomInt(1, 200),
				faker.RandomString(1),
				faker.RandomInt(5, 10),
			)
			res := vo.Address{}.From(addressFake)
			fmt.Printf("aquí %v", res)

			Expect(addressFake).To(Equal(res.String()))
		})
	})
})
