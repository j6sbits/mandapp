package vo_test

import (
	"mandapp/kit/vo"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("GeoHash", func() {
	Context("empty context", func() {
		It("", func() {
			geohash := vo.GeoHash{}.Default()

			Expect(geohash.IsZero()).Should(BeTrue())
		})
	})
})
