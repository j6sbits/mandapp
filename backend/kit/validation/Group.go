package validation

func IsThisdGroupValid(items ...Validator) bool {
	for index := range items {
		if !items[index].IsZero() {
			return false
		}
	}

	return true
}
