package command

import (
	"encoding/json"
	"mandapp/internal/order"
	"mandapp/kit/vo"
)

type ArriveOrder struct {
	OrderId    vo.UUID                    `json:"order_id"`
	CustomerId vo.UUID                    `json:"customer_id"`
	Events     order.OrderEventCollection `json:"events"`
	Amount     vo.Currency                `json:"amount"`
}

func (T ArriveOrder) ID() string {
	return "[Order] ArriveCommand"
}

func (T ArriveOrder) Data() []byte {
	jsonValue, _ := json.Marshal(t)
	return jsonValue
}

func (T ArriveOrder) IsZero() bool {
	return (T.OrderId.IsZero() &&
		T.CustomerId.IsZero() &&
		T.Events.IsZero() &&
		T.Amount.IsZero())
}
