package order_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"mandapp/internal/order"
	"mandapp/kit/vo"
)

var _ = Describe("InMemoryRepository", func() {
	var repo *order.InMemoryRepository

	BeforeEach(func() {
		repo = order.InMemoryRepository{}.Unique()
	})

	Context("given a new order that tries to be saved", func() {
		When("the repository performs a save operation on that order", func() {
			It("should not return errors", func() {
				newOrder := repo.CreateEmpty()
				newOrder.Arrive(
					vo.UUID{}.Random(),
					vo.UUID{}.Random(),
					order.OrderEventCollection{}.Default(),
					vo.Currency{}.From(1000),
				)
				err := repo.Save(newOrder)

				Expect(err).To(BeNil())
			})
		})
	})

	Context("given an order that was successfully stored", func() {
		When("the repository searchs for the id as the stored order", func() {
			It("should return the stored order without errors", func() {
				newOrder := repo.CreateEmpty()
				id := vo.UUID{}.Random()
				newOrder.Arrive(
					id,
					vo.UUID{}.Random(),
					order.OrderEventCollection{}.Default(),
					vo.Currency{}.From(1000),
				)
				repo.Save(newOrder)
				existsOrder, err := repo.GetById(id)

				Expect(err).To(BeNil())
				Expect(existsOrder.Cmp(newOrder)).To(BeNumerically("==", 0))
			})
		})
	})

	Context("given a stored order that attemps to be updated", func() {
		When("the repository runs the update operation on that order", func() {
			It("should replace the orden data without errors", func() {
				anOrder := repo.CreateEmpty()
				id := vo.UUID{}.Random()
				anOrder.Arrive(
					id,
					vo.UUID{}.Random(),
					order.OrderEventCollection{}.Default(),
					vo.Currency{}.From(1000),
				)
				repo.Save(anOrder)
				oldOrder, _ := repo.GetById(id)
				oldOrder.Taken(vo.UUID{}.Random())
				err := repo.Update(oldOrder)
				existsOrder, _ := repo.GetById(id)

				Expect(err).To(BeNil())
				Expect(anOrder.IsCreated()).To(BeTrue())
				Expect(existsOrder.IsInProgress()).To(BeTrue())
			})
		})
	})

})
