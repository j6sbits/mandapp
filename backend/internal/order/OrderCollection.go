package order

import (
	"encoding/json"
	"errors"
	"fmt"
)

type OrderCollection struct {
	data []Order
}

func (T OrderCollection) Default() OrderCollection {
	return OrderCollection{
		data: make([]Order, 0),
	}
}

func (T OrderCollection) IsZero() bool {
	return T.Size() == 0
}

func (T OrderCollection) From(raw []Order) OrderCollection {
	return OrderCollection{
		data: raw,
	}
}

func (T *OrderCollection) Add(obj Order) {
	T.data = append(T.data, obj)
}

func (T OrderCollection) Map(cb func(it Order) Order) OrderCollection {
	res := OrderCollection{}.Default()
	for _, val := range T.data {
		res.data = append(res.data, cb(val))
	}

	return res
}

func (T OrderCollection) Filter(fn func(it Order) bool) OrderCollection {
	res := OrderCollection{}.Default()
	for _, val := range T.data {
		if fn(val) {
			res.data = append(res.data, val)
		}
	}

	return res
}

func (T *OrderCollection) UnmarshalJSON(b []byte) error {
	type Alias []Order
	aux := Alias{}
	err := json.Unmarshal(b, &aux)
	if err != nil {
		return err
	}

	T.data = aux

	if !T.IsZero() {
		return nil
	}

	*T = T.Default()

	return errors.New(fmt.Sprintf("value doesn't match OrderCollection: %v", aux))
}

func (T OrderCollection) Size() int {
	return len(T.data)
}
