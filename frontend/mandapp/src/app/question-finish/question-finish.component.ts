import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-question-finish',
  templateUrl: './question-finish.component.html',
  styleUrls: ['./question-finish.component.scss']
})
export class QuestionFinishComponent implements OnInit {
  router: Router

  constructor(router: Router) { 
    this.router = router
  }

  ngOnInit(): void {
  }

  onScore() {
    this.router.navigateByUrl('/score')
  }

}
