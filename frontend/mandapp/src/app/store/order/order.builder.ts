import { Order } from './order';
import { Event } from './event'

export class OrderBuilder {
  private order: Order

  constructor() {
    this.order = new Order()
  }

  build() :Order {
    return this.order
  }

  withId(id: string) :OrderBuilder {
    this.order.id = id
    return this
  }

  withCustomerId(customerId: string) :OrderBuilder {
    this.order.customerId = customerId
    return this
  }

  withCourierId(courierId: string) :OrderBuilder {
    this.order.courierId = courierId
    return this
  }

  withEvents(events: Event[]) :OrderBuilder {
    this.order.events = events
    return this
  }

  withAmount(amount: number) :OrderBuilder {
    this.order.amount = amount
    return this
  }

  withScore(score: number) :OrderBuilder {
    this.order.score = score
    return this
  }
}