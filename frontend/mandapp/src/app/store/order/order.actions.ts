import { createAction, props } from '@ngrx/store'
import { Event } from './event'
import { Order } from './order'

export const addOrder = createAction('[Order Component] AddOrder', props<{ order: Order }>())
export const addEvent = createAction('[Order Component] AddEvent', props<{ ordIdx: number, event: Event }>())
export const delEvent = createAction('[Order Component] DelEvent', props<{ ordIdx: number, evtIdx: number }>())