import { Order } from './order';
import { createReducer, on, Action } from '@ngrx/store';
import { addOrder, addEvent, delEvent } from './order.actions';
import { state } from '@angular/animations';
import { Event } from './event';
import { OrderBuilder } from './order.builder';

export const initialState: Order[] = []

const orderReducer = createReducer(initialState,
  on(addOrder, (state, { order }) => {
    const newState: Order[] = [order].concat([...state])
    
    return newState
  }),

  on(addEvent, (state, { ordIdx, event }) => {
    const newState: Order[] = [...state]
    newState.forEach((ordIt, ordIdx_) => {
      if ( ordIdx_ == ordIdx && ordIt.events.length < 10 ) {
        ordIt.events = [].concat(ordIt.events, [event])
        ordIt.events = ordIt.events.map<Event>((it, idx) => {
          const newEvent: Event = {...it}
          newEvent.number = idx + 1
          return newEvent
        })
      }
    }) 

    return newState
  }),

  on(delEvent, (state, { ordIdx, evtIdx }) => {
    const newState: Order[] = [...state]
    newState.forEach((ordIt, idx_) => {
      if ( idx_ == ordIdx && ordIt.events.length > 2 ) {
        ordIt.events = ordIt.events.filter((_, evtIdx_) => evtIdx_ != evtIdx)
        ordIt.events = ordIt.events.map<Event>((evtIt, evtIdx_) => {
          const newEvent: Event = {...evtIt}
          newEvent.number = evtIdx_ + 1
          return newEvent
        })
      }
    })
      
    return newState
  })
)

export default function reducer(state: Order[] | undefined, action: Action) {
  return orderReducer(state, action)
}